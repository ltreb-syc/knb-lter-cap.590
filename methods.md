# Procedure

1. See Tank et al. 2006 Nutrient Diffusing Substrata (NDS) method.
2. Four agar treatments (control, +NaNO3, KH2PO4, + NaNO3 & KH2PO4) were prepared and poured into labeled Polycon cups (Maden Plastic's) with pre-drilled lids. Fritted glass disks with diameter of 2.7 cm were placed on top of agar and lids securely shut.
3. Cups were attached randomly to L-bars using silicone caulk and different colored zip-ties. One color was designated for each treatment to ensure sample recognition after incubation.
4. L-bars were wrapped in plastic wrap and kept in the refrigerator for no more than 36 hours.
5. L-bars were secured to the stream bed parallel to streamflow using tent stakes. 
6. Disks were allowed to accumulate biofilm in the stream. Incubation time is indicated by "Days_in_field" (at least 20 days if field is blank).
7. Chlorophyll a and pheophytin a were analyzed using a hot methanol extraction method (Tett et al. 1975, Tett et al. 1977). Absorbance values were measured using a Beckman DU® 530 Spectrophotometer. 

# Calculations

- Negative 750 nm turb numbers were changed to zero
- All absorbance values were background corrected

- Chl a (mg/m2) = (219.2 \times v \times ((ABS_666-ABS_750)-(ABS_666a-ABS_750a)))/A
- Pheophytin a(mg/m2) =  (219.2 \times v ((ABS_666a-ABS_750a \times 3.8)-(ABS_666-ABS_750)))/A

- 219.2 is the absorption coefficient for chlorophyll a in methanol \times 10 for conversion from ug/cm2 to mg/m2
- v is the extraction volume of solvent in mL
- A is area of fritted glass disk, 5.7255 cm2
- 3.8 is the maximum ratio of (ABS_666-ABS_750): (ABS_666a-ABS_750a) in the absence of pheopigments

# References

- Tank, J., Bernot, M., & Rosi-Marshall, E. 2006. Nitrogen limitation and uptake. Pg 213-236 in Hauer, F. R. Lamberti, G. A. (Eds.) Methods in stream ecology. 2nd edition. Academic Press, San Diego, CA.
- Tett, P., M. G. Kelly, and G. M. Hornberger. 1975. A method for the spectrophotometric measurement of chlorophyll and pheophytin a in benthic microalgae. Limnology and Ocreanography 20:887-896.
- Tett, P., M. G. Kelly, and G. M. Hornberger. 1977. Estimation of chlorophyll a and pheophytin a in methanol. Limnology and Oceanography 22:579-580.
